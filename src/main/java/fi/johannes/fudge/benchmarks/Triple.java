package fi.johannes.fudge.benchmarks;

import lombok.Data;

import java.math.BigInteger;

/**
 * johanness on 26/01/2017.
 */

@Data
public class Triple {
    private BigInteger a;
    private BigInteger b;
    private BigInteger c;

    boolean isValid;

    public Triple(BigInteger a, BigInteger b, BigInteger c) {

        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triple() {
    }

    protected void setValues(String a, String b, String c){
        this.a = new BigInteger(a);
        this.b = new BigInteger(b);
        this.c = new BigInteger(c);

        validate(a,b,c);
    }
    private void validate(String a, String b, String c){
        isValid = (a.length() > 1 && !this.a.equals(new BigInteger("0")) && a.equalsIgnoreCase(this.a.toString()));
        isValid = (b.length() > 1 && !this.b.equals(new BigInteger("0")) && a.equalsIgnoreCase(this.b.toString()));
        isValid = (c.length() > 1 && !this.c.equals(new BigInteger("0")) && a.equalsIgnoreCase(this.c.toString()));
        // TODO Move to method
    }

    @Override
    public String toString() {
        return "Triple{" +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", isValid=" + isValid +
                '}';
    }

    public static class TripleBuilder{
        String a;
        String b;
        String c;

        public TripleBuilder() {
        }

        public TripleBuilder withAll(String a, String b, String c){
            this.a = a;
            this.b = b;
            this.c = c;
            return this;
        }
        Triple build(){
            Triple triple = new Triple();
            triple.setValues(a,b,c);
            return triple;
        }
    }

}
