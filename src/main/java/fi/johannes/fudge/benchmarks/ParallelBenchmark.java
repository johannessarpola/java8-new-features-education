package fi.johannes.fudge.benchmarks;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * johanness on 26/01/2017.
 */
public class ParallelBenchmark {
    public static void main (String[] args) {
        int size = 10_000_000;

        List<Triple> list = new ArrayList<>(size);
        List<Triple> list2 = new ArrayList<>(size);

        Random r = new Random();

        for(int i = 0 ; i < size ; i++){
            Triple r1 = new Triple(randBigInt(BigInteger.ONE, r),randBigInt(BigInteger.ONE, r),randBigInt(BigInteger.ONE, r));
            Triple r2 = new Triple(randBigInt(BigInteger.ONE, r),randBigInt(BigInteger.ONE, r),randBigInt(BigInteger.ONE, r));
            list.add(r1);
            list2.add(r1);
        }
        parallelBenchmark(list);
        seqBenchmark(list);
        parallelBenchmark(list2);
        seqBenchmark(list2);


    }
    private static void seqBenchmark(List<Triple> list){
        long start1 = System.currentTimeMillis();
        List even = list.stream()
                .filter(t -> t.getA().add(t.getB().add(t.getC())).mod(new BigInteger("2")).equals(BigInteger.ZERO))
                .collect(Collectors.toList());
        long end1 = System.currentTimeMillis();
        System.out.println("Without parallel it took "+(end1-start1));
    }
    private static void parallelBenchmark (List<Triple> list){
        long start2 = System.currentTimeMillis();
        List even2 = list.stream()
                .parallel()
                .filter(t -> t.getA().add(t.getB().add(t.getC())).mod(new BigInteger("2")).equals(BigInteger.ZERO))
                .collect(Collectors.toList());
        long end2 = System.currentTimeMillis();
        System.out.println("With parallel it took "+(end2-start2));
    }
    private static BigInteger randBigInt(BigInteger n, Random rnd){
        BigInteger r;
        do {
            r = new BigInteger(n.bitLength(), rnd);
        } while (r.compareTo(n) >= 0);
        return r;
    }
}
