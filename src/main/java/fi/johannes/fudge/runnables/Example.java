package fi.johannes.fudge.runnables;

import fi.johannes.fudge.HelloRunnable;
import fi.johannes.fudge.RandomString;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * johanness on 26/01/2017.
 */
public class Example {
    public static void main(String[] args){
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        RandomString rs = new RandomString(10);
        Random ri = new Random();
        for(int i = 0; i < 10; i++) executorService.execute(new HelloRunnable(rs.nextString(), ri.nextInt(10)));
    }
}
