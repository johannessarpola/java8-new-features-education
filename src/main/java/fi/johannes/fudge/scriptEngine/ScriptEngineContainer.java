package fi.johannes.fudge.scriptEngine;

import fi.johannes.fudge.rest.ExampleTodos;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * johanness on 27/01/2017.
 */
public class ScriptEngineContainer {
    ScriptEngine engine;
    public ScriptEngineContainer(String filename) throws FileNotFoundException, ScriptException {
        ScriptEngineManager factory = new ScriptEngineManager();
        engine = factory.getEngineByName("JavaScript");
        engine.eval(new FileReader(filename));
    }
    private void hellowFromFile(ExampleTodos.Todo todo){
        try {
            ((Invocable)engine).invokeFunction("printing", todo);
        } catch (ScriptException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    private void fractFromFile(String mathSentence){
        try {
            ((Invocable)engine).invokeFunction("fancy_math", mathSentence);
        } catch (ScriptException | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }
    private void hellow(){
        try{
            engine.eval("print('Hello, World')");
        } catch(Exception ex){}
    }

    public static void main(String[] args) throws FileNotFoundException, ScriptException {
        ScriptEngineContainer container = new ScriptEngineContainer("hellow.js");
        container.hellow();
        container.hellowFromFile(new ExampleTodos.Todo(123, 1, "I'm todo", true));
        container.fractFromFile("23 / 11");
    }

}
