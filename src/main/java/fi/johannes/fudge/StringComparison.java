package fi.johannes.fudge;

import java.util.Arrays;
import java.util.Comparator;

/**
 * johanness on 26/01/2017.
 */
public class StringComparison {

    String[] arr = {"monday","tuesday","wednesday","thursday","friday","saturday","sunday"};

    public StringComparison(String[] arr) {
        this.arr = arr;
    }

    public StringComparison() {
    }

    public String[] sortedByLengthPower() {
        Comparator<String> comparator = (String a, String b) -> (int) (Math.pow(a.length(), 2) - Math.pow(b.length(), 2));
        Arrays.sort(arr, comparator);
        return arr;
    }
}

