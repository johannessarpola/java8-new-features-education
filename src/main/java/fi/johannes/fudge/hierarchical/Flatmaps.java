package fi.johannes.fudge.hierarchical;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * johanness on 27/01/2017.
 */
public class Flatmaps {

    public static void main(String[] args){


        Optional.of(new Outer())
                .flatMap(i -> Optional.ofNullable(i.inner))
                .flatMap(h -> Optional.ofNullable(h.heart))
                .flatMap(s -> Optional.ofNullable(s.spirit))
                .ifPresent(System.out::println);

        Optional.of(new OuterArr())
                .map(a -> Arrays.stream(a.arrs))
                .map(b -> b.map(c -> Arrays.stream(c.arrs)))
                .map(c -> c.map(d -> d.map(Integer::doubleValue).collect(Collectors.toList())))
                .ifPresent(o -> {
                    o.forEach(System.out::println);
                });
        Arrays.stream(new OuterArr().arrs).flatMap(i -> Arrays.stream(i.arrs)).forEach(System.out::println);
    }

    static class Outer {
        Inner inner = new Inner();
    }
    static class Inner {
        Heart heart = new Heart();
    }
    static class Heart {
        String spirit = "hello";
    }

    static class OuterArr {
        InnerArr[] arrs = {new InnerArr(), new InnerArr()};
    }
    static class InnerArr {
        Integer[] arrs = {1,2,3,4};
    }
}
