package fi.johannes.fudge;

import fi.johannes.fudge.cars.models.Car;
import fi.johannes.fudge.cars.CarFactory;
import fi.johannes.fudge.cars.models.Manufacturer;
import fi.johannes.fudge.cars.models.Model;
import fi.johannes.fudge.cars.predicates.WeightPredicate;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * johanness on 26/01/2017.
 */
public class Main {

    public static void main(String[] args){

        println();
        StringComparison sc = new StringComparison();
        Arrays.stream(sc.sortedByLengthPower()).forEach(System.out::println);

        println();
        CarFactory cf = new CarFactory(new Manufacturer("Hyundai", "hyundai@hyundai.com"));
        List<Car> cars = cf.buildBatch(10, new Model("Astra"));

        println();
        System.out.println("Light cars");
        cars.stream().filter(WeightPredicate::isLightWeight).forEach(c -> System.out.println(c.toString()));

        println();
        System.out.println("Medium cars");
        cars.stream().filter(WeightPredicate::isMediumWeight).forEach(c -> System.out.println(c.toString()));

        println();
        System.out.println("Heavy cars");
        cars.stream().filter(WeightPredicate::isHeavyWeight).forEach(c -> System.out.println(c.toString()));


        println();
        TetraParameterizable<String, Integer, Date> tp = (a,b,c) -> {
            System.out.println("Tetra: ");
            System.out.println(a);
            System.out.println(b.toString());
            System.out.println(c.toString());
        };
        tp.doSomething("Hello", new Integer(100), new Date());
        println();
    }
    static void println(){
        System.out.println("-------------------");
    }
}
