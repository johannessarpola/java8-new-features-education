package fi.johannes.fudge.cars;

import fi.johannes.fudge.cars.models.Model;

import java.util.*;

/**
 * johanness on 26/01/2017.
 */
// Function
// Predicate
// Supplier
public class ModelSelection {
    private List<Model> models = Arrays.asList(new Model("Carisma"), new Model("Corsa"));
    Random random;
    Iterator<Model> iterator;

    public ModelSelection() {
        iterator = models.iterator();
        random = new Random();
    }

    public List<Model> getModels() {
        return models;
    }
    public Model getSomeModel(){
        return models.get(random.nextInt(models.size()));
    }

    public Model getNext(){
        if(!iterator.hasNext()){
            iterator = models.iterator();
        }
        return iterator.next();
    }



}
