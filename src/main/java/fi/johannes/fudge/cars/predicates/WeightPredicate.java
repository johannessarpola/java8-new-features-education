package fi.johannes.fudge.cars.predicates;

import fi.johannes.fudge.cars.models.Car;

/**
 * johanness on 26/01/2017.
 */
public class WeightPredicate {
    private static Long lightThreshold = 1100L;
    private static Long mediumThreshold = 1300L;
    private static Long heavyThreshold = 1500L;

    public static boolean isLightWeight(Car c){
        long x = c.getWeight();
        return x <= mediumThreshold;
    }
    public static boolean isMediumWeight(Car c){
        long x = c.getWeight();
        return x > lightThreshold && x <= heavyThreshold;
    }
    public static boolean isHeavyWeight(Car c){
        long x = c.getWeight();
        return x > heavyThreshold;
    }

}
