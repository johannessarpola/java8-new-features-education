package fi.johannes.fudge.cars.consumers;

import fi.johannes.fudge.cars.models.Car;
import fi.johannes.fudge.cars.models.Color;

import java.util.Random;
import java.util.function.Consumer;

/**
 * johanness on 26/01/2017.
 */
public class CarRandomizedPainterConsumer implements Consumer<Car>{
    private Random random = new Random();

    @Override
    public void accept(Car car) {
        // Set random color
        int index = random.nextInt(Color.values().length);
        Color c = Color.values()[index];
        car.setColor(c);
    }
}
