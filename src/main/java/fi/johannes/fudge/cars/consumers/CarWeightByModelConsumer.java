package fi.johannes.fudge.cars.consumers;

import fi.johannes.fudge.cars.WeightResolver;
import fi.johannes.fudge.cars.models.Car;

import java.util.function.Consumer;

/**
 * johanness on 26/01/2017.
 */
public class CarWeightByModelConsumer implements Consumer<Car>{
    @Override
    public void accept(Car car) {
        long weight = WeightResolver.resolveWeight(car.getModel());
        car.setWeight(weight);
    }
}
