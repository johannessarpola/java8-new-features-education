package fi.johannes.fudge.cars.consumers;

import fi.johannes.fudge.cars.models.Car;

import java.time.LocalDateTime;
import java.util.function.Consumer;

/**
 * johanness on 26/01/2017.
 */
public class CarManufacturingYearConsumer implements Consumer<Car> {
    @Override
    public void accept(Car car) {
        car.setManufacturingYear(LocalDateTime.now().getYear());
    }
}
