package fi.johannes.fudge.cars;

import fi.johannes.fudge.cars.consumers.CarManufacturingYearConsumer;
import fi.johannes.fudge.cars.consumers.CarRandomizedModelConsumer;
import fi.johannes.fudge.cars.consumers.CarRandomizedPainterConsumer;
import fi.johannes.fudge.cars.consumers.CarWeightByModelConsumer;
import fi.johannes.fudge.cars.models.Car;
import fi.johannes.fudge.cars.models.Manufacturer;
import fi.johannes.fudge.cars.models.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * johanness on 26/01/2017.
 */
public class CarFactory {
    final Manufacturer manufacturer;
    List<Consumer> parts;

    public CarFactory(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }
    public List<Car> buildBatch(Integer count, Model model){
        List<Car> batch = emptyBatch(count);

        CarRandomizedPainterConsumer painterConsumer = new CarRandomizedPainterConsumer();
        CarRandomizedModelConsumer modelConsumer = new CarRandomizedModelConsumer();
        CarManufacturingYearConsumer manufacturingYearConsumer = new CarManufacturingYearConsumer();
        CarWeightByModelConsumer weightByModelConsumer = new CarWeightByModelConsumer();
        List<Car> collect = batch
                .stream()
                .parallel()
                .map(c -> {
                    c.setModel(model);
                    c.setIsFaulty(false); // Own consumer
                    c.setManufacturer(manufacturer);
                    painterConsumer.accept(c);
                    manufacturingYearConsumer.accept(c);
                    weightByModelConsumer.accept(c);
                    return c;
                })
                .filter(Car::getIsOk)
                .collect(Collectors.toList());
        return collect;
    }

    private List<Car> emptyBatch(Integer count) {
        List<Car> cars = new ArrayList<>(count);
        for(int i = 0; i < count ; i++ ){
            cars.add(new Car());
        }
        return cars;
    }
}
