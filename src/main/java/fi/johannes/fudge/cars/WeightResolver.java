package fi.johannes.fudge.cars;

import fi.johannes.fudge.cars.models.Model;

import java.util.Random;

/**
 * johanness on 26/01/2017.
 */
public class WeightResolver {
    public static long resolveWeight(Model model){
        return new Random().nextInt(800)+1000L;
    }

}
