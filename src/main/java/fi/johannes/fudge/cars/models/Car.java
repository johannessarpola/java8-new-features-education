package fi.johannes.fudge.cars.models;

import lombok.Data;

/**
 * johanness on 26/01/2017.
 */
@Data
public class Car {

    private Manufacturer manufacturer;
    private int manufacturingYear;
    private Color color;
    private Model model;
    private Boolean isFaulty;
    private int cylinders;
    private long weight;

    public Boolean getIsOk() {
        return !isFaulty;
    }

}
