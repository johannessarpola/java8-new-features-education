package fi.johannes.fudge.cars.models;

import lombok.Data;

/**
 * johanness on 26/01/2017.
 */
@Data
public class Model {
    private String model;

    public Model(String model) {
        this.model = model;
    }
}
