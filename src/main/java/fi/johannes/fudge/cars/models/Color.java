package fi.johannes.fudge.cars.models;

/**
 * johanness on 26/01/2017.
 */
public enum Color {
    Red,
    Green,
    Blue,
    Purple,
    Grey,
    LightGrey

}
