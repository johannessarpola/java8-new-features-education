package fi.johannes.fudge.cars.models;

import lombok.Data;

/**
 * johanness on 26/01/2017.
 */
@Data
public class Manufacturer {
    private String name;
    private String contact;

    public Manufacturer(String name, String contact) {
        this.name = name;
        this.contact = contact;
    }
}
