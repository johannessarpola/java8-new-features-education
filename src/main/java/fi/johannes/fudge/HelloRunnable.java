package fi.johannes.fudge;

/**
 * johanness on 26/01/2017.
 */
public class HelloRunnable implements  Runnable{
    String name;
    int iterations;


    public HelloRunnable(String name, int iterations) {
        this.name = name;
        this.iterations = iterations;
    }

    @Override
    public void run() {
        for(int i = 0; i < iterations ; i++){
            System.out.println("Hello from "+name+" "+i);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(String.format("%1$s i'm done after %2$d!", name, iterations));

    }
}
