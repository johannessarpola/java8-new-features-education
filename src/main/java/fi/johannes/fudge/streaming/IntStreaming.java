package fi.johannes.fudge.streaming;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * johanness on 27/01/2017.
 */
public class IntStreaming {
public static  void main(String[] args){
    List<Integer> li = IntStream.range(0, 1000)
                                .map(i -> {
                                    i = (int) (Math.random() * 1000);
                                    return i;
                                })
                                .boxed()
                                .collect(Collectors.toList());

    li.forEach(i -> System.out.print(i + ","));
    li.parallelStream().sorted().forEach(System.out::println);
    System.out.println("---------------------------------");
    li.stream().parallel().sorted().sequential().forEach(System.out::println);
}
}
