package fi.johannes.fudge.dates;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * johanness on 27/01/2017.
 */
public class Dates {
    public static void main(String[] args){
        LocalDateTime ldt = LocalDateTime.now();
        LocalDateTime xmas = LocalDateTime.of(2017, 12,24,0,0);
        System.out.print(ldt.until(xmas, ChronoUnit.HOURS));
    }
}
