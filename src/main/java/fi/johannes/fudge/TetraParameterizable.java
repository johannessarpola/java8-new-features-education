package fi.johannes.fudge;

import java.util.function.Consumer;

/**
 * johanness on 26/01/2017.

 */

@FunctionalInterface
public interface TetraParameterizable<T1, T2, T3> {

    void doSomething(T1 a, T2 b, T3 c);

    default void doSomethingMultiple(int n, T1 a, T2 b, T3 c){
        for(int i = 0 ; i<n ; i++){
            doSomething(a, b, c);
        }
    }

    default TetraParameterizable<T1,T2,T3> before(Consumer<String> sc){
        sc.accept("...And then...");
        return this;
    }


    static <T1, T2, T3> String concatToStr(T1 a, T2 b, T3 c){
        StringBuilder sb = new StringBuilder();
        sb.append(a.toString());
        sb.append(b.toString());
        sb.append(c.toString());
        return sb.toString();
    }

}
