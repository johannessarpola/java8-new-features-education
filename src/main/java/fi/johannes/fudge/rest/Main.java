package fi.johannes.fudge.rest;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * johanness on 26/01/2017.
 */
public class Main {

    public static void main(String[] args){
        List<ExampleTodos.Todo> todos = new ExampleTodos().get();

        Map<Long, Long> collect = todos.stream()
                .filter(ExampleTodos.Todo::isNotDone)
                .collect(Collectors.groupingBy(ExampleTodos.Todo::getUserId, Collectors.counting()));
        for(Map.Entry<Long, Long> counts : collect.entrySet()){
            System.out.println(counts.getKey() + " : "+counts.getValue());
        }
    }

}

