package fi.johannes.fudge.rest;

import com.google.gson.Gson;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

/**
 * johanness on 26/01/2017.
 */
public class ExampleTodos implements Supplier<List<ExampleTodos.Todo>>{
    private String url = "https://jsonplaceholder.typicode.com/todos";

    public ExampleTodos() {

    }
    @Override
    public List<Todo> get() {
        try {
            return doTheCall();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<Todo> doTheCall() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = br.readLine()) != null) {
            result.append(line);
        }
        return Arrays.asList(deserialize(result.toString()));
    }

    public Todo[] deserialize(String todos){
        Gson gson = new Gson();
        return gson.fromJson(todos, Todo[].class);
    }

    public class Todos {
        List<Todo> todos;
    }

    @Data
    public static class Todo {
        long userId;
        long id;
        String title;
        boolean completed;

        public Todo(long userId, long id, String title, boolean completed) {
            this.userId = userId;
            this.id = id;
            this.title = title;
            this.completed = completed;
        }

        public Todo() {
        }
        public boolean isNotDone(){
            return !completed;
        }

        @Override
        public String toString() {
            return "Todo{" +
                    "userId=" + userId +
                    ", id=" + id +
                    ", title='" + title + '\'' +
                    ", completed=" + completed +
                    '}';
        }
    }
}

