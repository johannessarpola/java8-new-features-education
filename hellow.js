/**
 * Created by johanness on 27/01/2017.
 */

load("jslib/math.js");

function printing(a){
    // print goes to system.out through some wrapper
    print(a.toString());
}
function fancy_math(sentence){
    var e = math.eval(sentence)
    var fraction = math.fraction(e)
    print(math.format(fraction, {fraction: 'ratio'}));
}